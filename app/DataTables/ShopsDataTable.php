<?php

namespace App\DataTables;

use App\Models\Shop;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class ShopsDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->addColumn('image', function ($row) {
                return $row->image ? "<img src =" . asset('uploads/shop/' . $row->image) . " width='60'> " : "N/A";
            })->addColumn('status', function ($row) {
                return view('backend.common.status', [
                    'id' => $row->id, 'panel' => 'shop', 'status' => $row->status
                ]);
            })
            ->addColumn('action', function ($row) {
                return view('backend.common.buttons', ['id' => $row->id, 'panel' => 'shop']);
            })
            ->rawColumns(['image']);
    }

    public function query(Shop $model)
    {
        return $model->newQuery();
    }

    public function html()
    {
        return $this->builder()
            ->setTableId('coin-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('create'),
                Button::make('reload')
            );
    }

    protected function getColumns()
    {
        return [
            Column::computed('DT_RowIndex')
                ->title('SN'),
            Column::make('image'),
            Column::make('title'),
            Column::make('price'),
            Column::make('status')
                ->addClass('text-center'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(100)
                ->addClass('text-center'),
        ];
    }

    protected function filename()
    {
        return 'Coins_' . date('YmdHis');
    }
}
