<?php

namespace App\DataTables;

use App\Models\Tournament;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class TournamentsDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->addColumn('image', function ($row) {
                return $row->image ? "<img src =" . asset('uploads/tournament/' . $row->image) . " width='60'> " : "N/A";
            })
            ->addColumn('game_image', function ($row) {
                return $row->game_image ? "<img src =" . asset('uploads/tournament/' . $row->game_image) . " width='60'> " : "N/A";
            })
            ->addColumn('teams', function ($row) {
                $team = [];
                foreach ($row->teams as $mem) {
                    array_push($team, $mem->name);
                }
                return count($team);
            })
            ->addColumn('status', function ($row) {
                return view('backend.common.status', [
                    'id' => $row->id, 'panel' => 'tournament', 'status' => $row->status
                ]);
            })
            ->addColumn('action', function ($row) {
                $startButton = "<a href=" . route('tournament.start', $row->id) . " class='btn btn-xs btn-warning'>start</a>";
                $viewButton = "<a href=" . route('tournament.show', $row->id) . " class='btn btn-xs btn-outline-primary'><i class='fa fa-eye'></i></a>";
                $buttons = view('backend.common.buttons', ['id' => $row->id, 'panel' => 'tournament']);
                return $viewButton . $buttons . $startButton;
            })
            ->rawColumns(['image', 'game_image', 'action']);
    }

    public function query(Tournament $model)
    {
        return $model->newQuery();
    }

    public function html()
    {
        return $this->builder()
            ->setTableId('tournaments-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('create'),
                Button::make('reload')
            );
    }

    protected function getColumns()
    {
        return [
            Column::computed('DT_RowIndex')
                ->title('SN'),
            Column::make('image'),
            Column::make('name'),
            Column::make('game_image'),
            Column::make('game'),
            Column::make('teams'),
            Column::make('status')
                ->addClass('text-center'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(100)
                ->addClass('text-center'),
        ];
    }

    protected function filename()
    {
        return 'Tournaments_' . date('YmdHis');
    }
}
