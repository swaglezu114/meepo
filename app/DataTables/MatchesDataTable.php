<?php

namespace App\DataTables;

use App\Models\OnGoingGame;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class MatchesDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->addColumn('tournament', function ($row) {
                return $row->tournament->name;
            })
            ->addColumn('first_team', function ($row) {
                return $row->firstTeam->name;
            })
            ->addColumn('second_team', function ($row) {
                return $row->secondTeam->name;
            })
            ->addColumn('winner', function ($row) {
                return $row->winnerTeam->name;
            })
            ->addColumn('action', function ($row) {
                return "<a href=" . route('match.edit', $row->id) . " class='btn btn-sm btn-warning'><i class='fa fa-eye'></i></a>";
            });
    }

    public function query(OnGoingGame $model)
    {
        return $model->newQuery();
    }

    public function html()
    {
        return $this->builder()
            ->setTableId('match-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('reload')
            );
    }

    protected function getColumns()
    {
        return [
            Column::computed('DT_RowIndex')
                ->title('SN'),
            Column::make('tournament'),
            Column::make('first_team'),
            Column::make('second_team'),
            Column::make('winner'),
            Column::make('coin'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(100)
                ->addClass('text-center'),
        ];
    }

    protected function filename()
    {
        return 'Coins_' . date('YmdHis');
    }
}
