<?php

namespace App\DataTables;

use App\Models\User;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class PlayersDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->addColumn('coin', function ($row) {
                return $row->number_of_coins;
            })
            ->addColumn('image', function ($row) {
                return $row->image ? "<img src =" . asset('uploads/user/' . $row->image) . " width='60'> " : "N/A";
            })
            ->addColumn('action', function ($row) {
                return view('backend.common.buttons', ['id' => $row->id, 'panel' => 'user']);
            })
            ->rawColumns(['image']);
    }

    public function query(User $model)
    {
        return $model->newQuery()
            ->where('type', 3)
            ->where('id', '!=', auth()->user()->id);
    }

    public function html()
    {
        return $this->builder()
            ->setTableId('users-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('create'),
                Button::make('reload')
            );
    }

    protected function getColumns()
    {
        return [
            Column::computed('DT_RowIndex')
                ->title('SN'),
            Column::make('image'),
            Column::make('name'),
            Column::make('email'),
            Column::make('coin'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(100)
                ->addClass('text-center'),
        ];
    }

    protected function filename()
    {
        return 'Users_' . date('YmdHis');
    }
}
