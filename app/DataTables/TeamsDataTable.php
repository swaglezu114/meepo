<?php

namespace App\DataTables;

use App\Models\Team;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class TeamsDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->addColumn('logo', function ($row) {
                return $row->logo ? "<img src =" . asset('uploads/team/' . $row->logo) . " width='60'> " : "N/A";
            })
            ->addColumn('members', function ($row) {
                $member = [];
                foreach ($row->users as $mem) {
                    array_push($member, $mem->name);
                }
                return count($member) . '/5';
            })
            ->addColumn('status', function ($row) {
                return view('backend.common.status', [
                    'id' => $row->id, 'panel' => 'team', 'status' => $row->status
                ]);
            })
            ->addColumn('action', function ($row) {
                return view('backend.common.buttons', ['id' => $row->id, 'panel' => 'team']);
            })
            ->rawColumns(['logo']);
    }

    public function query(Team $model)
    {
        return $model->newQuery()->with('users');
    }

    public function html()
    {
        return $this->builder()
            ->setTableId('teams-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('create'),
                Button::make('reload')
            );
    }

    protected function getColumns()
    {
        return [
            Column::computed('DT_RowIndex')
                ->title('SN'),
            Column::make('logo'),
            Column::make('name'),
            Column::make('game'),
            Column::make('members'),
            Column::make('status')
                ->addClass('text-center'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(100)
                ->addClass('text-center'),
        ];
    }

    protected function filename()
    {
        return 'Teams_' . date('YmdHis');
    }
}
