<?php

namespace App\Http\Requests\Tournament;

use App\Http\Requests\BaseRequest;

class StoreRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'main_image' => 'max:2048',
            'game_image' => 'max:2048',
            'name' => 'required|max:100',
            'game' => 'required|max:100',
            'reward_point' => 'required',
        ];
    }
}
