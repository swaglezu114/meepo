<?php

namespace App\Http\Requests\Team;

use App\Http\Requests\BaseRequest;

class StoreRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'name' => 'required|max:100'
        ];
    }
}
