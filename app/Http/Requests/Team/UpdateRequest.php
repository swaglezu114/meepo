<?php

namespace App\Http\Requests\Team;

use App\Http\Requests\BaseRequest;

class UpdateRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'name' => 'required|max:100'
        ];
    }
}
