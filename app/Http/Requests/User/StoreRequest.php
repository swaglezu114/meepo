<?php

namespace App\Http\Requests\User;

use App\Http\Requests\BaseRequest;

class StoreRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'main_image' => 'max:2048',
            'name' => 'required|max:100',
            'email' => 'required|max:100',
            'password' => 'required|max:100',
        ];
    }
}
