<?php

namespace App\Http\Requests\Shop;

use App\Http\Requests\BaseRequest;

class UpdateRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'main_image' => 'max:2048',
            'title' => 'required|max:100',
            'price' => 'required|max:10',
        ];
    }
}
