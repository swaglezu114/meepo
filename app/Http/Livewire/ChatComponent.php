<?php

namespace App\Http\Livewire;

use App\Models\Chat;
use Livewire\Component;

class ChatComponent extends Component
{
    public $message;

    public function render()
    {
        return view('backend.player.chat.includes.chat');
    }

    protected $rules = [
        'message' => 'required',
    ];

    public function saveMessage()
    {
        $this->validate();
        $chat = new Chat();
        $chat->user_id = auth()->user()->id;
        $chat->message = $this->message;
        $chat->save();
        $this->message = '';
    }
}
