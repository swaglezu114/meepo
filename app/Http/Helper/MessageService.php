<?php

namespace App\Http\Helper;

class MessageService
{
    public function successMessageToDisplay($signal, $panel, $message)
    {
        alert()->$signal($panel . ' ' . $message . ' successfully !!')->toToast();
    }

    public function errorMessage($signal, $message)
    {
        alert()->$signal($message)->toToast();
    }
}
