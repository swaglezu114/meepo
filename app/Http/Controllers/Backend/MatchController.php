<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\MatchesDataTable;
use App\Http\Helper\MessageService;
use App\Models\OnGoingGame;
use App\Models\Team;
use App\Models\UserTournament;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MatchController extends BaseController
{
    protected $baseRoute = 'match';
    protected $viewPath = 'backend.match';
    protected $panel = 'Match';
    protected $folder = 'match';
    private $messageService;

    public function __construct(MessageService $messageService)
    {
        $this->messageService = $messageService;
    }

    public function index(MatchesDataTable $matchesDataTable)
    {
        return $matchesDataTable->render(parent::loadDataToView($this->viewPath . '.index'));
    }

    public function create()
    {
        $data = [];
        $data['model'] = new OnGoingGame();
        return view(parent::loadDataToView($this->viewPath . '.create'), compact('data'));
    }

    public function store(Request $request)
    {
        OnGoingGame::create($request->all());
        $this->messageService->successMessageToDisplay('success', $this->panel, 'started');
        return redirect()->route($this->baseRoute . '.index');
    }

    public function show($id)
    {
        $data = [];
        $data['row'] = OnGoingGame::where('id', $id)->first();
        return view(parent::loadDataToView($this->viewPath . '.show'), compact('data'));
    }

    public function edit($id)
    {
        $data = [];
        $data['row'] = OnGoingGame::where('id', $id)->first();
        return view(parent::loadDataToView($this->viewPath . '.edit'), compact('data'));
    }

    public function update(Request $request, $id)
    {
        try {
            $match = OnGoingGame::where('id', $id)->first();
            $match->update($request->all());
            $team = Team::where('id', $request->get('winner'))->first();
            foreach ($team->users as $user) {
                $user->update([
                    'number_of_coins' => $user->number_of_coins + $request->get('coin') / $team->users()->count()
                ]);
            }
            foreach ($team->users as $user) {
                UserTournament::create([
                    'user_id' => $user->id,
                    'tournament_id' => $request->get('winner'),
                    'coin' => $request->get('coin') / $team->users()->count(),
                ]);
            }
            $this->messageService->successMessageToDisplay('success', $this->panel, 'updated');
            return redirect()->route($this->baseRoute . '.index');
        } catch (\Exception $exception) {
            $this->messageService->successMessageToDisplay('success', '', $exception->getMessage());
            return redirect()->route($this->baseRoute . '.index');
        }
    }
}
