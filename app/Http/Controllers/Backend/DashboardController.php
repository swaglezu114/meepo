<?php

namespace App\Http\Controllers\Backend;

use App\Models\Shop;
use App\Models\Team;
use App\Models\Tournament;
use App\Models\User;

class DashboardController extends BaseController
{
    public function index()
    {
        $data = [];
        $data['tournaments'] = Tournament::count();
        $data['teams'] = Team::count();
        $data['players'] = User::where('type', 3)->count();
        $data['shopItems'] = Shop::count();
        return view('backend.dashboard.index', compact('data'));
    }
}
