<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\TeamsDataTable;
use App\Http\Helper\MessageService;
use App\Http\Requests\Team\StoreRequest;
use App\Http\Requests\Team\UpdateRequest;
use App\Models\Team;
use App\Models\TeamMember;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TeamController extends BaseController
{
    protected $baseRoute = 'team';
    protected $viewPath = 'backend.team';
    protected $panel = 'Team';
    protected $folder = 'team';
    protected $folder_path;

    private $messageService;

    public function __construct(MessageService $messageService)
    {
        $this->messageService = $messageService;
        $this->folder_path = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $this->folder . DIRECTORY_SEPARATOR;
    }

    public function index(TeamsDataTable $teamsDataTable)
    {
        return $teamsDataTable->render(parent::loadDataToView($this->viewPath . '.index'));
    }

    public function create()
    {
        $data = [];
        $data['model'] = new Team();
        return view(parent::loadDataToView($this->viewPath . '.create'), compact('data'));
    }

    public function store(StoreRequest $request)
    {
        if ($request->has('main_logo')) {
            $file = $request->file('main_logo');
            $fileName = Str::random(25) . '.' . $file->getClientOriginalExtension();
            $file->move($this->folder_path, $fileName);
            $request->merge(['logo' => $fileName]);
        }
        Team::create($request->all());
        $this->messageService->successMessageToDisplay('success', $this->panel, 'added');
        return redirect()->route($this->baseRoute . '.index');
    }

    public function edit($id)
    {
        $data = [];
        $data['row'] = Team::where('id', $id)->first();
        return view(parent::loadDataToView($this->viewPath . '.edit'), compact('data'));
    }

    public function update(UpdateRequest $request, $id)
    {
        $team = Team::where('id', $id)->first();
        if ($request->has('main_logo')) {
            $file = $request->file('main_logo');
            $fileName = Str::random(25) . '.' . $file->getClientOriginalExtension();
            $file->move($this->folder_path, $fileName);
            $request->merge(['logo' => $fileName]);

            if ($team->logo) {
                if (file_exists($this->folder_path . $team->logo)) {
                    unlink($this->folder_path . $team->logo);
                }
            }
        }
        $team->update($request->all());
        $this->messageService->successMessageToDisplay('success', $this->panel, 'updated');
        return redirect()->route($this->baseRoute . '.index');
    }

    public function addTeamMember(Request $request)
    {
        TeamMember::create([
            'team_id' => $request->get('team_id'),
            'user_id' => auth()->user()->id,
        ]);
        $this->messageService->successMessageToDisplay('success', $this->panel, 'updated');
        return redirect()->route($this->baseRoute . '.index');
    }

    public function destroy($id)
    {
        $team = Team::where('id', $id)->first();
        if ($team->logo) {
            if (file_exists($this->folder_path . $team->logo)) {
                unlink($this->folder_path . $team->logo);
            }
        }
        $team->delete();
        $this->messageService->successMessageToDisplay('success', $this->panel, 'deleted');
        return redirect()->route($this->baseRoute . '.index');
    }

    public function status($id)
    {
        $team = Team::where('id', $id)->first();
        $team->status = !$team->status;
        $team->save();
        $this->messageService->successMessageToDisplay('success', $this->panel, 'updated');
        return redirect()->route($this->baseRoute . '.index');
    }

    public function createTeam(StoreRequest $request)
    {
        $teamMember = TeamMember::select('user_id')->where('user_id', auth()->user()->id)->first();
        if ($teamMember === null) {
            if ($request->has('main_logo')) {
                $file = $request->file('main_logo');
                $fileName = time() . '.' . $file->getClientOriginalExtension();
                $file->move($this->folder_path, $fileName);
                $request->merge(['logo' => $fileName]);
            }
            $team = Team::create($request->all());
            TeamMember::create([
                'team_id' => $team->id,
                'user_id' => auth()->user()->id,
            ]);
        } else {
            alert()->warning('you have already joined other team !!')->toToast();
        }
        return redirect()->route('player.team');
    }
}
