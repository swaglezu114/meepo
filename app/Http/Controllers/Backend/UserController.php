<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\PlayersDataTable;
use App\DataTables\UsersDataTable;
use App\Http\Helper\MessageService;
use App\Http\Requests\User\StoreRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Models\User;
use Illuminate\Support\Str;

class UserController extends BaseController
{
    protected $baseRoute = 'user.index';
    protected $viewPath = 'backend.user';
    protected $panel = 'User';
    protected $folder = 'user';
    protected $folder_path;
    private $messageService;

    public function __construct(MessageService $messageService)
    {
        $this->messageService = $messageService;
        $this->folder_path = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $this->folder . DIRECTORY_SEPARATOR;

    }

    public function index(UsersDataTable $usersDataTable)
    {
        return $usersDataTable->render(parent::loadDataToView($this->viewPath . '.index'));
    }

    public function player(PlayersDataTable $playersDataTable)
    {
        return $playersDataTable->render(parent::loadDataToView($this->viewPath . '.index'));
    }

    public function create()
    {
        $data = [];
        $data['model'] = new User();
        return view(parent::loadDataToView($this->viewPath . '.create'), compact('data'));
    }

    public function store(StoreRequest $request)
    {
        if ($request->has('main_image')) {
            $file = $request->file('main_image');
            $fileName = Str::random(25) . '.' . $file->getClientOriginalExtension();
            $file->move($this->folder_path, $fileName);
            $request->merge(['image' => $fileName]);
        }
        User::create($request->all());
        $this->messageService->successMessageToDisplay('success', $this->panel, 'added');
        return redirect()->route($this->baseRoute);
    }

    public function edit($id)
    {
        $data = [];
        $data['row'] = User::where('id', $id)->first();
        return view(parent::loadDataToView($this->viewPath . '.edit'), compact('data'));
    }

    public function update(UpdateRequest $request, $id)
    {
        $user = User::where('id', $id)->first();
        if ($request->has('main_image')) {
            $file = $request->file('main_image');
            $fileName = Str::random(25) . '.' . $file->getClientOriginalExtension();
            $file->move($this->folder_path, $fileName);
            $request->merge(['image' => $fileName]);

            if ($user->image) {
                if (file_exists($this->folder_path . $user->image)) {
                    unlink($this->folder_path . $user->image);
                }
            }
        }
        $user->update($request->all());
        $this->messageService->successMessageToDisplay('success', $this->panel, 'updated');
        return redirect()->route($this->baseRoute);
    }

    public function destroy($id)
    {
        $user = User::where('id', $id)->first();
        if ($user->image) {
            if (file_exists($this->folder_path . $user->image)) {
                unlink($this->folder_path . $user->image);
            }
        }
        $user->delete();
        $this->messageService->successMessageToDisplay('success', $this->panel, 'deleted');
        return redirect()->route($this->baseRoute);
    }

    public function status($id)
    {
        $user = User::where('id', $id)->first();
        $user->status = !$user->status;
        $user->save();
        $this->messageService->successMessageToDisplay('success', $this->panel, 'updated');
        return redirect()->route($this->baseRoute);
    }
}
