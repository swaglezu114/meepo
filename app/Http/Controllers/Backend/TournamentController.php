<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\TournamentsDataTable;
use App\Http\Helper\MessageService;
use App\Http\Requests\Tournament\StoreRequest;
use App\Http\Requests\Tournament\UpdateRequest;
use App\Models\OnGoingGame;
use App\Models\Tournament;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TournamentController extends BaseController
{
    protected $baseRoute = 'tournament';
    protected $viewPath = 'backend.tournament';
    protected $panel = 'Tournament';
    protected $folder = 'tournament';
    protected $folder_path;

    private $messageService;

    public function __construct(MessageService $messageService)
    {
        $this->messageService = $messageService;
        $this->folder_path = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $this->folder . DIRECTORY_SEPARATOR;
    }

    public function index(TournamentsDataTable $tournamentsDataTable)
    {
        return $tournamentsDataTable->render(parent::loadDataToView($this->viewPath . '.index'));
    }

    public function create()
    {
        $data = [];
        $data['model'] = new Tournament();
        return view(parent::loadDataToView($this->viewPath . '.create'), compact('data'));
    }

    public function store(StoreRequest $request)
    {
        if ($request->has('main_image')) {
            $file = $request->file('main_image');
            $fileName = Str::random(25) . '.' . $file->getClientOriginalExtension();
            $file->move($this->folder_path, $fileName);
            $request->merge(['image' => $fileName]);
        }
        if ($request->has('main_game_image')) {
            $gameImageFile = $request->file('main_game_image');
            $gameImageFilename = Str::random(25) . '.' . $gameImageFile->getClientOriginalExtension();
            $gameImageFile->move($this->folder_path, $gameImageFilename);
            $request->merge(['game_image' => $gameImageFilename]);
        }
        Tournament::create($request->all());
        $this->messageService->successMessageToDisplay('success', $this->panel, 'added');
        return redirect()->route($this->baseRoute . '.index');
    }

    public function show($id)
    {
        $data = [];
        $data['row'] = Tournament::where('id', $id)
            ->with('teams')
            ->first();
        return view(parent::loadDataToView($this->viewPath . '.show'), compact('data'));
    }

    public function edit($id)
    {
        $data = [];
        $data['row'] = Tournament::where('id', $id)->first();
        return view(parent::loadDataToView($this->viewPath . '.edit'), compact('data'));
    }

    public function update(UpdateRequest $request, $id)
    {
        $tournament = Tournament::where('id', $id)->first();
        if ($request->has('main_image')) {
            $file = $request->file('main_image');
            $fileName = Str::random(25) . '.' . $file->getClientOriginalExtension();
            $file->move($this->folder_path, $fileName);
            $request->merge(['image' => $fileName]);

            if ($tournament->image) {
                if (file_exists($this->folder_path . $tournament->image)) {
                    unlink($this->folder_path . $tournament->image);
                }
            }

        }
        if ($request->has('main_game_image')) {
            $gameImageFile = $request->file('main_game_image');
            $gameImageFilename = Str::random(25) . '.' . $gameImageFile->getClientOriginalExtension();
            $gameImageFile->move($this->folder_path, $gameImageFilename);
            $request->merge(['game_image' => $gameImageFilename]);

            if ($tournament->game_image) {
                if (file_exists($this->folder_path . $tournament->game_image)) {
                    unlink($this->folder_path . $tournament->game_image);
                }
            }

        }
        $tournament->update($request->all());
        $this->messageService->successMessageToDisplay('success', $this->panel, 'updated');
        return redirect()->route($this->baseRoute . '.index');
    }

    public function destroy($id)
    {
        $tournament = Tournament::where('id', $id)->first();
        if ($tournament->image) {
            if (file_exists($this->folder_path . $tournament->image)) {
                unlink($this->folder_path . $tournament->image);
            }
        }
        if ($tournament->game_image) {
            if (file_exists($this->folder_path . $tournament->game_image)) {
                unlink($this->folder_path . $tournament->game_image);
            }
        }
        $tournament->delete();
        $this->messageService->successMessageToDisplay('success', $this->panel, 'deleted');
        return redirect()->route($this->baseRoute . '.index');
    }

    public function status($id)
    {
        $tournament = Tournament::where('id', $id)->first();
        $tournament->status = !$tournament->status;
        $tournament->save();
        $this->messageService->successMessageToDisplay('success', $this->panel, 'updated');
        return redirect()->route($this->baseRoute . '.index');
    }

    public function start($id)
    {
        $data = [];
        $data['row'] = Tournament::findOrFail($id)
            ->with('teams')
            ->first();
        return view(parent::loadDataToView($this->viewPath . '.start'), compact('data'));
    }

    public function storeMatch(Request $request)
    {
        try {
            if ($request->get('first_team') === $request->get('second_team')) {
                throw new \Exception('both the teams are same !!');
            }
            OnGoingGame::create($request->all());
            $this->messageService->successMessageToDisplay('success', '', 'match started');
            return redirect()->route('match.index');
        } catch (\Exception $exception) {
            $this->messageService->errorMessage('error', $exception->getMessage());
            return redirect()->route('match.index');
        }
    }
}
