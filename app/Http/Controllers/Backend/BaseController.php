<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

class BaseController extends Controller
{
    protected function loadDataToView($viewPath)
    {
        View::composer($viewPath, function ($view) {
            $view->with('baseRoute', $this->baseRoute);
            $view->with('viewPath', $this->viewPath);
            $view->with('panel', $this->panel);
        });
        return $viewPath;
    }
}
