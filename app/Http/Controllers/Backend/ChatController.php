<?php

namespace App\Http\Controllers\Backend;

use App\Events\ChatEvent;
use App\Http\Controllers\Controller;
use App\Models\Chat;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    public function index()
    {
        return view('backend.player.chat.chat');
    }

    public function fetchAllMessages()
    {
        return Chat::with('user')->get();
    }

    public function sendMessage(Request $request)
    {
        $chat = auth()->user()->messages()->create([
            'message' => $request->get('message')
        ]);
        broadcast(new ChatEvent($chat->load('user')))->toOthers();

        return ['status' => 'success'];
    }
}
