<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Shop;
use App\Models\Chat;
use App\Models\Team;
use App\Models\TeamMember;
use App\Models\TeamTournament;
use App\Models\Tournament;
use App\Models\User;
use App\Models\UserShop;
use App\Models\UserTournament;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PlayerController extends Controller
{
    protected $viewPath = 'backend.player';

    protected $folder = 'user';
    protected $folder_path;

    public function __construct()
    {
        $this->folder_path = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $this->folder . DIRECTORY_SEPARATOR;
    }

    public function dashboard()
    {
        $data = [];
        $data['coins'] = Shop::where('status', 1)->get();
        $data['userShop'] = UserShop::where('user_id', auth()->user()->id)->get();
        $data['teamUser'] = TeamMember::where('user_id', auth()->user()->id)->first();
        $data['userTournament'] = UserTournament::where('user_id', auth()->user()->id)->get();
        return view($this->viewPath . '.dashboard.index', compact('data'));
    }

    public function chat()
    {
        $data = [];
        $data['messages'] = Chat::select('id', 'user_id', 'message')->get();
        return view($this->viewPath . '.chat.index', compact('data'));
    }

    public function profile($id)
    {
        $data = [];
        $data['row'] = User::where('id', $id)->first();
        return view($this->viewPath . '.profile.index', compact('data'));
    }

    public function updateProfile(Request $request, $id)
    {
        $user = User::where('id', $id)->first();
        if ($request->has('main_image')) {
            $file = $request->file('main_image');
            $fileName = Str::random(25) . '.' . $file->getClientOriginalExtension();
            $file->move($this->folder_path, $fileName);
            $request->merge(['image' => $fileName]);

            if ($user->image) {
                if (file_exists($this->folder_path . $user->image)) {
                    unlink($this->folder_path . $user->image);
                }
            }
        }
        $user->update($request->all());
        return redirect()->route('player.dashboard');
    }

    public function shop()
    {
        $data = [];
        $data['shops'] = Shop::where('status', 1)->get();
        return view($this->viewPath . '.shop.index', compact('data'));
    }

    public function team(Request $request)
    {
        $data = [];
        $data['team'] = Team::with('users')
            ->where('name', 'LIKE', '%' . $request->get('team') . '%')
            ->paginate(5);
        return view($this->viewPath . '.team.index', compact('data'));
    }

    public function joinTeam($id)
    {
        $data = [];
        $data['row'] = Team::where('id', $id)->first();
        return view($this->viewPath . '.team.join', compact('data'));
    }

    public function storeTeam(Request $request)
    {
        $teamMember = TeamMember::select('user_id')->where('user_id', auth()->user()->id)->first();
        if ($teamMember === null) {
            TeamMember::create([
                'team_id' => $request->get('team_id'),
                'user_id' => auth()->user()->id,
            ]);
            alert()->success('you have joined the team successfully !!')->toToast();
        } else {
            alert()->info('you have already joined other team !!')->toToast();
        }
        return redirect()->route('player.team');
    }

    public function tournament(Request $request)
    {
        $data = [];
        $data['tournaments'] = Tournament::where('name', 'LIKE', '%' . $request->get('tournament') . '%')
            ->paginate(5);
        return view($this->viewPath . '.tournament.index', compact('data'));
    }

    public function joinTournament($id)
    {
        $data = [];
        $data['row'] = Tournament::where('id', $id)->first();
        $data['myTeam'] = TeamMember::where('user_id', auth()->user()->id)->first();
        return view($this->viewPath . '.tournament.join', compact('data'));
    }

    public function storeTournament(Request $request)
    {
        $tournamentMemberTeam = TeamTournament::select('team_id')->where('team_id', $request->get('team_id'))->first();
        if ($tournamentMemberTeam === null) {
            TeamTournament::create([
                'team_id' => $request->get('team_id'),
                'tournament_id' => $request->get('tournament_id'),
            ]);
        }
        alert()->success('you have joined the tournament successfully !!')->toToast();
        return redirect()->route('player.tournament');
    }

    public function shopItem(Request $request)
    {
        if (auth()->user()->number_of_coins > $request->get('costCoin')) {
            auth()->user()->update([
                'number_of_coins' => auth()->user()->number_of_coins - $request->get('costCoin')
            ]);
            UserShop::create([
                'user_id' => auth()->user()->id,
                'shop_id' => $request->get('shop_id'),
                'coin' => $request->get('costCoin'),
            ]);
            alert()->success('you have successfully purchased !!')->toToast();
            return redirect()->back();
        }
        alert()->warning('you do not have enough coins !!')->toToast();
        return redirect()->back();
    }
}
