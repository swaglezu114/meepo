<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\ShopsDataTable;
use App\Http\Helper\MessageService;
use App\Http\Requests\Shop\StoreRequest;
use App\Http\Requests\Shop\UpdateRequest;
use App\Models\Shop;
use Illuminate\Support\Str;

class ShopController extends BaseController
{
    protected $baseRoute = 'shop';
    protected $viewPath = 'backend.shop';
    protected $panel = 'Shop';
    protected $folder = 'shop';
    protected $folder_path;

    private $messageService;

    public function __construct(MessageService $messageService)
    {
        $this->messageService = $messageService;
        $this->folder_path = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $this->folder . DIRECTORY_SEPARATOR;
    }

    public function index(ShopsDataTable $shopsDataTable)
    {
        return $shopsDataTable->render(parent::loadDataToView($this->viewPath . '.index'));
    }

    public function create()
    {
        $data = [];
        $data['model'] = new Shop();
        return view(parent::loadDataToView($this->viewPath . '.create'), compact('data'));
    }

    public function store(StoreRequest $request)
    {
        if ($request->has('main_image')) {
            $file = $request->file('main_image');
            $fileName = Str::random(25) . '.' . $file->getClientOriginalExtension();
            $file->move($this->folder_path, $fileName);
            $request->merge(['image' => $fileName]);
        }
        Shop::create($request->all());
        $this->messageService->successMessageToDisplay('success', $this->panel, 'added');
        return redirect()->route($this->baseRoute . '.index');
    }

    public function edit($id)
    {
        $data = [];
        $data['row'] = Shop::where('id', $id)->first();
        return view(parent::loadDataToView($this->viewPath . '.edit'), compact('data'));
    }

    public function update(UpdateRequest $request, $id)
    {
        $shop = Shop::where('id', $id)->first();
        if ($request->has('main_image')) {
            $file = $request->file('main_image');
            $fileName = Str::random(25) . '.' . $file->getClientOriginalExtension();
            $file->move($this->folder_path, $fileName);
            $request->merge(['image' => $fileName]);

            if ($shop->image) {
                if (file_exists($this->folder_path . $shop->image)) {
                    unlink($this->folder_path . $shop->image);
                }
            }
        }
        $shop->update($request->all());
        $this->messageService->successMessageToDisplay('success', $this->panel, 'updated');
        return redirect()->route($this->baseRoute . '.index');
    }

    public function destroy($id)
    {
        $shop = Shop::where('id', $id)->first();
        $shop->delete();
        $this->messageService->successMessageToDisplay('success', $this->panel, 'deleted');
        return redirect()->route($this->baseRoute . '.index');
    }

    public function status($id)
    {
        $shop = Shop::where('id', $id)->first();
        $shop->status = !$shop->status;
        $shop->save();
        $this->messageService->successMessageToDisplay('success', $this->panel, 'updated');
        return redirect()->route($this->baseRoute . '.index');
    }
}
