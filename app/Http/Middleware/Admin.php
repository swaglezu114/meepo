<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Admin
{
    public function handle(Request $request, Closure $next)
    {
        if (auth()->user()->type === 1 || auth()->user()->type === 2 || auth()->user()->type === 3) {
            return $next($request);
        } else {
            abort(404);
        }
    }
}
