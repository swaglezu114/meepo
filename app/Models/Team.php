<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use HasFactory;

    protected $fillable = [
        'logo',
        'name',
        'game',
        'status',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function tournaments()
    {
        return $this->belongsToMany(Tournament::class);
    }
}
