<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{
    use HasFactory;

    protected $fillable = [
        'image',
        'name',
        'game_image',
        'game',
        'reward_point',
        'prize',
        'team_quantity',
        'registration_start_date',
        'registration_end_date',
        'tournament_start_date',
        'description',
        'status',
    ];

    public function teams()
    {
        return $this->belongsToMany(Team::class);
    }
}
