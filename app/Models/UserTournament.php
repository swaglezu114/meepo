<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTournament extends Model
{

    protected $table = 'user_tournament';

    protected $fillable = [
        'user_id',
        'tournament_id',
        'coin',
    ];

    public function tournament()
    {
        return $this->belongsTo(Tournament::class);
    }
}
