<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OnGoingGame extends Model
{
    use HasFactory;

    protected $table = 'matches';

    protected $fillable = [
        'tournament_id',
        'first_team',
        'score_by_first_team',
        'second_team',
        'score_by_second_team',
        'winner',
        'coin',
        'status',
    ];

    public function tournament()
    {
        return $this->belongsTo(Tournament::class)->withDefault();
    }

    public function firstTeam()
    {
        return $this->belongsTo(Team::class, 'first_team');
    }

    public function secondTeam()
    {
        return $this->belongsTo(Team::class, 'second_team');
    }

    public function winnerTeam()
    {
        return $this->belongsTo(Team::class, 'winner')->withDefault();
    }
}
