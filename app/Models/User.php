<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $fillable = [
        'image',
        'name',
        'email',
        'phone',
        'profession',
        'khalti',
        'password',
        'type',
        'number_of_coins',
        'status',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function myTeam()
    {
        return $this->belongsTo(TeamMember::class, 'team_id');
    }

    public function teams()
    {
        return $this->belongsToMany(Team::class);
    }

    public function messages()
    {
        return $this->hasMany(Chat::class);
    }

    public function shops()
    {
        return $this->hasMany(Shop::class);
    }
}
