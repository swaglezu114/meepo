<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    public function run()
    {
        User::create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => 'admin',
            'type' => 1,
        ]);
        User::create([
            'name' => 'organizer',
            'email' => 'organizer@organizer.com',
            'password' => 'organizer',
            'type' => 2,
        ]);
        User::create([
            'name' => 'player',
            'email' => 'player@player.com',
            'password' => 'player',
            'type' => 3,
        ]);
    }
}
