<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::view('/', 'welcome');

Route::group(['namespace' => 'Backend', 'middleware' => ['auth']], function () {

    Route::get('/messages', 'ChatController@fetchAllMessages');
    Route::post('/messages', 'ChatController@sendMessage');

    Route::post('team/add/team/member', 'TeamController@addTeamMember')->name('team.add.team.member');
    Route::resource('team', 'TeamController');
    Route::get('team/status/{id}', 'TeamController@status')->name('team.status');

    Route::post('team/create', 'TeamController@createTeam')->name('team.create');

    Route::group(['middleware' => [/*'admin', 'organizer'*/]], function () {

        Route::get('dashboard', 'DashboardController@index')->name('dashboard');
        Route::get('player', 'UserController@player')->name('player');

        Route::resource('user', 'UserController');
        Route::get('user/status/{id}', 'UserController@status')->name('user.status');

        Route::resource('shop', 'ShopController');
        Route::get('shop/status/{id}', 'ShopController@status')->name('shop.status');

        Route::resource('tournament', 'TournamentController');
        Route::get('tournament/status/{id}', 'TournamentController@status')->name('tournament.status');
        Route::get('tournament/{id}/start', 'TournamentController@start')->name('tournament.start');
        Route::post('tournament/{id}/start', 'TournamentController@storeMatch')->name('tournament.start.store');

        Route::resource('match', 'MatchController');
    });

    Route::group(['prefix' => 'player', 'as' => 'player.', 'middleware' => ['player']], function () {

//        Route::get('chats', 'ChatController@index')->name('chats');

        Route::get('dashboard', 'PlayerController@dashboard')->name('dashboard');
        Route::get('chat', 'PlayerController@chat')->name('chat');
        Route::get('profile/{id}', 'PlayerController@profile')->name('profile');
        Route::put('profile/{id}', 'PlayerController@updateProfile')->name('profile.update');
        Route::get('team', 'PlayerController@team')->name('team');

        Route::get('join/team/{id}', 'PlayerController@joinTeam')->name('join.team');
        Route::post('join/team', 'PlayerController@storeTeam')->name('join.team.store');

        Route::get('join/tournament/{id}', 'PlayerController@joinTournament')->name('join.tournament');
        Route::post('join/tournament', 'PlayerController@storeTournament')->name('join.tournament.store');

        Route::get('tournament', 'PlayerController@tournament')->name('tournament');
        Route::get('shop', 'PlayerController@shop')->name('shop');

        Route::post('buy', 'PlayerController@shopItem')->name('buy');
    });

});


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
