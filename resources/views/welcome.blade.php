<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Meepo</title>

    <link rel="stylesheet" href="{{ asset('player/asset/css/style.css') }}"/>

    <link rel="stylesheet" href="{{ asset('player/asset/css/font-awesome/css/font-awesome.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('player/asset/css/lightbox.css') }}"/>

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800,300italic,400italic'
          rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,700italic,700,400italic'
          rel='stylesheet' type='text/css'>

    <script src="{{ assert('player/asset/js/jquery-1.9.1.js') }}" type="text/javascript"></script>
    <script src="{{ assert('player/asset/js/js.js') }}" type="text/javascript"></script>
    <!-- Important Owl stylesheet -->
    <link rel="stylesheet" href="{{ assert('player/asset/owl-carousel/owl.carousel.css') }}"/>
    <!-- Default Theme -->
    <link rel="stylesheet" href="{{ assert('player/asset/owl-carousel/owl.theme.css') }}"/>
    <!-- Include js plugin -->
    <script src="{{ assert('player/asset/owl-carousel/owl.carousel.js') }}"></script>
    <script src="{{ assert('player/asset/js/countdown.js') }}" type="text/javascript" charset="utf-8"></script>
    <script src="{{ assert('player/asset/js/lightbox.js') }}" type="text/javascript" charset="utf-8"></script>
</head>
<body>

<header>
    <nav>
        <div class="logo">
            <a href="/"><img src="{{ asset('player/asset/img/meepo.png') }}" alt="Sport Template"></a>
        </div>

        <ul>
            <li class="active"><a href="index.html">Home</a></li>
            @guest
                <li><a href="{{ route('login') }}">Login</a></li>
                <li><a href="{{ route('register') }}">Register</a></li>
            @else
                @if(auth()->user()->type === 1 || auth()->user()->type === 2)
                    <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                @else
                    <li><a href="{{ route('player.dashboard') }}">Dashboard</a></li>
                @endif
            @endguest
        </ul>
    </nav>


    <div class="clearfix header-content">
        <h1>Meepo ! GAME ON</h1>
        <h2>LIVE IN YOUR WORLD. PLAY IN OURS.</h2>
        <p>Play Games. Play With Others And Change the Game.<br> Dont Let The Game Change You.</p>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
    </div>
    

</header>





<script src="{{ asset('player/asset/js/fluidvids.js') }}"></script>
</body>
</html>
