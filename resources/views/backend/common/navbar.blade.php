<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <!-- Messages Dropdown Menu -->
        <li class="nav-item mr-3">
            <a href="#" class="dropdown-item">
                welcome <strong>{{ auth()->user()->name }}</strong>
            </a>
        </li>
        <li class="nav-item">
            <a href="#" class="dropdown-item" onclick="event.preventDefault();$('#logout-form').submit();">
                <i class="fa fa-sign-out-alt"></i> Logout
            </a>
        </li>
        <form id="logout-form" action="{{ route('logout') }}" method="POST">
            @csrf
        </form>
    </ul>
</nav>
