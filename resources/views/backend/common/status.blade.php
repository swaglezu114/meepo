<a href="{{ route($panel.'.status',$id) }}">
    @if($status)
        <button class="btn btn-sm btn-primary">
            Active
        </button>
    @else
        <button class="btn btn-sm btn-warning">
            In-Active
        </button>
    @endif
</a>
