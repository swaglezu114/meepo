<footer class="main-footer">
    <!-- Default to the left -->
    <strong>Copyright &copy; {{ now()->format('Y') }} <a href="/">{{ config('app.name') }}</a>.</strong> All rights
    reserved.
</footer>
