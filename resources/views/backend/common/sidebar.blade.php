<aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
        <img src="{{ asset('assets/backend/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo"
             class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">{{ config('app.name') }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="{{ route('dashboard') }}"
                       class="{!! request()->is('dashboard')?'nav-link active':'nav-link' !!}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('user.index') }}"
                       class="{!! request()->is('user*')?'nav-link active':'nav-link' !!}">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            User
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('shop.index') }}"
                       class="{!! request()->is('shop*')?'nav-link active':'nav-link' !!}">
                        <i class="nav-icon fas fa-coins"></i>
                        <p>
                            Shop
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('team.index') }}"
                       class="{!! request()->is('team*')?'nav-link active':'nav-link' !!}">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            Team
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('tournament.index') }}"
                       class="{!! request()->is('tournament*')?'nav-link active':'nav-link' !!}">
                        <i class="nav-icon fas fa-gamepad"></i>
                        <p>
                            Tournament
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('match.index') }}"
                       class="{!! request()->is('match*')?'nav-link active':'nav-link' !!}">
                        <i class="nav-icon fas fa-magic"></i>
                        <p>
                            Match
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
