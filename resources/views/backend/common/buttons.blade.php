<a href="{{ route($panel.'.edit',$id) }}" class="btn btn-xs btn-secondary">
    <i class="fa fa-pen"></i>
</a>
<form action="{{ route($panel.'.destroy',$id) }}" method="POST">
    @csrf
    @method('DELETE')
    <button type="submit" class="btn btn-xs btn-danger">
        <i class="fa fa-trash"></i>
    </button>
</form>
