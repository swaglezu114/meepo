@extends('backend.common.master')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ $panel }} Manager</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route($baseRoute.'.index') }}">{{ $panel }}</a></li>
                        <li class="breadcrumb-item active">Edit</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <!-- /.card-header -->
                    <div class="card-body">
                        {!! Form::model($data['row'], ['route' => [$baseRoute.'.update',$data['row']->id], 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
                        @csrf
                        <table class="table">
                            <tr>
                                <th>First team</th>
                                <td>
                                    {{ $data['row']->firstTeam->name }} <br>
                                    {!! Form::number('score_by_first_team', null, ['placeholder' => 'score by first team', 'min'=> 0, 'class' => 'form-control col-md-4']) !!}
                                </td>
                            </tr>
                            <tr>
                                <th>Second team</th>
                                <td>
                                    {{ $data['row']->secondTeam->name }} <br>
                                    {!! Form::number('score_by_second_team', null, ['placeholder' => 'score by second team', 'min'=> 0, 'class' => 'form-control col-md-4']) !!}
                                </td>
                            </tr>
                            <tr>
                                <th>Select winner</th>
                                <td>
                                    {!! Form::radio('winner', $data['row']->first_team); !!} {{ $data['row']->firstTeam->name }}
                                    <br>
                                    {!! Form::radio('winner', $data['row']->second_team); !!} {{ $data['row']->secondTeam->name }}
                                </td>
                            </tr>
                            <tr>
                                <th>Winner coin</th>
                                <td>
                                    {!! Form::number('coin', null, ['min' => 0, 'class' => 'form-control col-md-4', 'placeholder' => 'winner prize in coins']) !!}
                                </td>
                            </tr>
                        </table>
                        <div class="text-center card-footer">
                            <button type="submit" class="btn btn-danger">Declare winner</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
