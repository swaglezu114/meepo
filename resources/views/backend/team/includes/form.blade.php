@if(isset($data['row']))
    <div class="form-group row">
        <label for="main_logo" class="col-sm-2 col-form-label">Existing Logo</label>
        <div class="col-sm-8">
            @if(isset($data['row']->logo))
                <img src="{{ asset('uploads/team/'.$data['row']->logo) }}" width="100" alt="">
            @else
                {!! Form::text('text', 'N/A', ['class' => 'form-control', 'disabled']) !!}
            @endif
        </div>
    </div>
@endif

<div class="form-group row">
    <label for="main_logo" class="col-sm-2 col-form-label">Logo</label>
    <div class="col-sm-8">
        {!! Form::file('main_logo', ['class' => 'form-control']) !!}
        @error('main_logo')
        <span class="error-message" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="name" class="col-sm-2 col-form-label">Name</label>
    <div class="col-sm-8">
        {!! Form::text('name', null, ['placeholder' => 'Name', 'class' => 'form-control']) !!}
        @error('name')
        <span class="error-message" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="game" class="col-sm-2 col-form-label">Select Game</label>
    <div class="col-sm-8">
        {!! Form::select('game', ['CS GO' => 'CS GO', 'DOTA' => 'DOTA'], null, ['placeholder' => 'Select Game', 'class' => 'form-control']) !!}
        @error('game')
        <span class="error-message" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>
