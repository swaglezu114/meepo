@extends('backend.common.master')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ $panel }} Manager</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('team.index') }}">{{ $panel }}</a></li>
                        <li class="breadcrumb-item active">Edit {{ $panel }}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Join {{ $panel }}</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">

                        <div class="card-body">
                            <table class="table">
                                <tr>
                                    <th>Logo</th>
                                    <td>
                                        <img src="{{ asset('uploads/teams/'.$data['row']->logo) }}" width="100"
                                             alt="">
                                    </td>
                                </tr>
                                <tr>
                                    <th>Name</th>
                                    <td>
                                        {{ $data['row']->name }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Members</th>
                                    <td>
                                        @foreach($data['row']->users as $mem)
                                            {{ $mem->name }}@if(!$loop->last), @endif
                                        @endforeach
                                    </td>
                                </tr>
                            </table>
                        </div>
                        {!! Form::model($data['row'], ['route' => [$baseRoute.'.add.team.member'], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal']) !!}
                        @csrf
                        <input type="hidden" name="team_id" value="{{ $data['row']->id }}">
                        <div class="card-footer text-center">
                            <button type="submit" class="btn btn-info"><i class="fa fa-check"></i> Join Team</button>
                            <a href="{{ route($baseRoute.'.index') }}" class="btn btn-default"><i
                                    class="fa fa-undo"></i> Back</a>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
@section('js')

@endsection
