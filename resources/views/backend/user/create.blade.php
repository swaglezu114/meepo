@extends('backend.common.master')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ $panel }} Manager</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('user.index') }}">{{ $panel }}</a></li>
                        <li class="breadcrumb-item active">Add {{ $panel }}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Add User</h3>
                    </div>
                    <div class="card-body">
                        {!! Form::model($data['model'], ['route' => ['user.store'], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal']) !!}
                        @csrf
                        <div class="card-body">
                            @include('backend.user.includes.form')
                        </div>
                        <div class="card-footer text-center">
                            <button type="submit" class="btn btn-info"><i class="fa fa-check"></i> Submit</button>
                            <button type="reset" class="btn btn-default">Cancel</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
@section('js')

@endsection
