@if(isset($data['row']))
    <div class="form-group row">
        <label for="main_logo" class="col-sm-2 col-form-label">Existing Image</label>
        <div class="col-sm-8">
            @if(isset($data['row']->image))
                <img src="{{ asset('uploads/user/'.$data['row']->image) }}" width="100" alt="">
            @else
                {!! Form::text('text', 'N/A', ['class' => 'form-control', 'disabled']) !!}
            @endif
        </div>
    </div>
@endif

<div class="form-group row">
    <label for="image" class="col-sm-2 col-form-label">Images</label>
    <div class="col-sm-8">
        {!! Form::file('main_image', ['class' => 'form-control']) !!}
        @error('main_image')
        <span class="error-message" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="name" class="col-sm-2 col-form-label">Name</label>
    <div class="col-sm-8">
        {!! Form::text('name', null, ['placeholder' => 'Name', 'class' => 'form-control']) !!}
        @error('name')
        <span class="error-message" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="email" class="col-sm-2 col-form-label">Email</label>
    <div class="col-sm-8">
        {!! Form::email('email', null, ['placeholder' => 'Email', 'class' => 'form-control']) !!}
        @error('email')
        <span class="error-message" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="phone" class="col-sm-2 col-form-label">Number</label>
    <div class="col-sm-8">
        {!! Form::number('phone', null, ['placeholder' => 'Number', 'class' => 'form-control']) !!}
        @error('phone')
        <span class="error-message" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="profession" class="col-sm-2 col-form-label">Profession</label>
    <div class="col-sm-8">
        {!! Form::text('profession', null, ['placeholder' => 'Profession', 'class' => 'form-control']) !!}
        @error('profession')
        <span class="error-message" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="khalti" class="col-sm-2 col-form-label">khalti ID</label>
    <div class="col-sm-8">
        {!! Form::text('khalti', null, ['placeholder' => 'khalti ID', 'class' => 'form-control']) !!}
        @error('khalti')
        <span class="error-message" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="password" class="col-sm-2 col-form-label">Password</label>
    <div class="col-sm-8">
        <input type="password" name="password" class="form-control" id="password"
               placeholder="Password">
        @error('password')
        <span class="error-message" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>
