@if(isset($data['row']))
    <div class="col-sm-6">
        <div class="form-group">
            <label for="main_logo">Existing Image</label>
            @if(isset($data['row']->image))
                <img src="{{ asset('uploads/tournament/'.$data['row']->image) }}" width="100" alt="">
            @else
                {!! Form::text('text', 'N/A', ['class' => 'form-control', 'disabled']) !!}
            @endif
        </div>
    </div>
@endif

@if(isset($data['row']))
    <div class="col-sm-6">
        <div class="form-group">
            <label for="main_logo">Existing Image</label>
            @if(isset($data['row']->game_image))
                <img src="{{ asset('uploads/tournament/'.$data['row']->game_image) }}" width="100" alt="">
            @else
                {!! Form::text('text', 'N/A', ['class' => 'form-control', 'disabled']) !!}
            @endif
        </div>
    </div>
@endif

<div class="col-sm-6">
    <div class="form-group">
        <label for="main_image">Image</label>
        {!! Form::file('main_image', ['class' => 'form-control']) !!}
        @error('main_image')
        <span class="error-message" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="col-sm-6">
    <div class="form-group">
        <label for="main_game_image">Game Image</label>
        {!! Form::file('main_game_image', ['class' => 'form-control']) !!}
        @error('main_game_image')
        <span class="error-message" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="col-sm-6">
    <div class="form-group">
        <label for="name">Reward Point</label>
        {!! Form::number('reward_point', null, ['placeholder' => 'Reward Point', 'class' => 'form-control']) !!}
        @error('reward_point')
        <span class="error-message" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>


<div class="col-sm-6">
    <div class="form-group">
        <label for="name">Name</label>
        {!! Form::text('name', null, ['placeholder' => 'Name', 'class' => 'form-control']) !!}
        @error('name')
        <span class="error-message" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>


<div class="col-sm-6">
    <div class="form-group">
        <label for="game">Select game</label>
        {!! Form::select('game', ['DOTA' =>'DOTA', 'CSGO' => 'CSGO'], null, ['placeholder' => 'Select game', 'class' => 'form-control']) !!}
        @error('game')
        <span class="error-message" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>



<div class="col-sm-6">
    <div class="form-group">
        <label for="team_quantity">Maximum Team Qty</label>
        {!! Form::select('team_quantity', [4 => 'Four', '8' => 'Eight',], null, ['placeholder' => 'Team Qty', 'class' => 'form-control']) !!}
        @error('team_quantity')
        <span class="error-message" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>


<div class="col-sm-6">
    <div class="form-group">
        <label for="tournament_start_date">Tournament Start Date</label>
        {!! Form::date('tournament_start_date', null, ['placeholder' => 'Tournament Start Date', 'class' => 'form-control']) !!}
        @error('tournament_start_date')
        <span class="error-message" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="col-sm-12">
    <div class="form-group">
        <label for="prize">Prize</label>
        {!! Form::textarea('prize', null, ['placeholder' => 'Prize', 'class' => 'form-control']) !!}
        @error('prize')
        <span class="error-message" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="col-sm-12">
    <div class="form-group">
        <label for="description">Description</label>
        {!! Form::textarea('description', null, ['placeholder' => 'Description', 'class' => 'form-control']) !!}
        @error('description')
        <span class="error-message" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>
