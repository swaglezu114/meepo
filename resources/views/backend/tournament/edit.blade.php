@extends('backend.common.master')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ $panel }} Manager</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route($baseRoute.'.index') }}">{{ $panel }}</a></li>
                        <li class="breadcrumb-item active">Edit Form</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <!-- /.card-header -->
                    <div class="card-body">
                        {!! Form::model($data['row'], ['route' => [$baseRoute.'.update',$data['row']->id], 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
                        @csrf
                        <div class="row">
                            @include($viewPath.'.includes.form')
                        </div>
                        <div class="card-footer text-center">
                            <button type="submit" class="btn btn-info"><i class="fa fa-check"></i> Update</button>
                            <button type="reset" class="btn btn-default">Cancel</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
@section('js')

@endsection
