@extends('backend.common.master')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ $panel }} Manager</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route($baseRoute.'.index') }}">{{ $panel }}</a></li>
                        <li class="breadcrumb-item active">Start</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="table">
                            <tr>
                                <th>Image</th>
                                <td>
                                    @if(isset($data['row']->image))
                                        <img src="{{ asset('uploads/tournament/'.$data['row']->image) }}" width="75"
                                             alt="">
                                    @else
                                        N/A
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Name</th>
                                <td>{{ $data['row']->name }}</td>
                            </tr>
                            <tr>
                                <th>Game Image</th>
                                <td>
                                    @if(isset($data['row']->game_image))
                                        <img src="{{ asset('uploads/tournament/'.$data['row']->game_image) }}"
                                             width="75" alt="">
                                    @else
                                        N/A
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Game</th>
                                <td>{{ $data['row']->game }}</td>
                            </tr>
                            <tr>
                                <th>Registered Teams</th>
                                <td>
                                    @foreach($data['row']->teams as $tournamentTeam)
                                        {{ $tournamentTeam->name }}
                                        <strong>({{ $tournamentTeam->users()->count() }})</strong>
                                        <br>
                                    @endforeach
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <div class="card">
                    <div class="card-body">
                        <form action="" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="text" name="tournament_id" value="{{ $data['row']->id }}"
                                   class="form-control" hidden>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="first_team">Select first team</label>
                                        <select name="first_team" class="form-control">
                                            <option value="">Select first team</option>
                                            @foreach($data['row']->teams as $tournamentTeam)
                                                <option
                                                    value="{{ $tournamentTeam->id }}">{{ $tournamentTeam->name }}
                                                    <strong>({{ $tournamentTeam->users()->count() }})</strong></option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="second_team">Select second team</label>
                                        <select name="second_team" class="form-control">
                                            <option value="">Select second team</option>
                                            @foreach($data['row']->teams as $tournamentTeam)
                                                <option
                                                    value="{{ $tournamentTeam->id }}">{{ $tournamentTeam->name }}
                                                    <strong>({{ $tournamentTeam->users()->count() }})</strong></option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-center">
                                <button type="submit" class="btn btn-primary">Start tournament</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
