@extends('backend.common.master')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ $panel }} Manager</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route($baseRoute.'.index') }}">{{ $panel }}</a></li>
                        <li class="breadcrumb-item active">Show {{ $panel }}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="table">
                            <tr>
                                <th>Image</th>
                                <td>
                                    @if(isset($data['row']->image))
                                        <img src="{{ asset('uploads/tournament/'.$data['row']->image) }}" width="75"
                                             alt="">
                                    @else
                                        N/A
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Name</th>
                                <td>{{ $data['row']->name }}</td>
                            </tr>
                            <tr>
                                <th>Game Image</th>
                                <td>
                                    @if(isset($data['row']->game_image))
                                        <img src="{{ asset('uploads/tournament/'.$data['row']->game_image) }}"
                                             width="75" alt="">
                                    @else
                                        N/A
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Game</th>
                                <td>{{ $data['row']->game }}</td>
                            </tr>
                            <tr>
                                <th>Registered Teams</th>
                                <td>
                                    @foreach($data['row']->teams as $tournamentTeam)
                                        <span class="badge badge-secondary">{{ $tournamentTeam->name }}</span>
                                    @endforeach
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
