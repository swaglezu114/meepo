<div class="form-group row">
    <label for="main_image" class="col-sm-2 col-form-label">Image</label>
    <div class="col-sm-8">
        {!! Form::file('main_image', ['class' => 'form-control']) !!}
        @error('main_image')
        <span class="error-message" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="title" class="col-sm-2 col-form-label">Title</label>
    <div class="col-sm-8">
        {!! Form::text('title', null, ['placeholder' => 'Title', 'class' => 'form-control', 'required']) !!}
        @error('title')
        <span class="error-message" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="price" class="col-sm-2 col-form-label">Price</label>
    <div class="col-sm-8">
        {!! Form::number('price', null, ['placeholder' => 'Price', 'class' => 'form-control', 'required', 'min' => 0]) !!}
        @error('price')
        <span class="error-message" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="description" class="col-sm-2 col-form-label">Description</label>
    <div class="col-sm-8">
        {!! Form::textarea('description', null, ['placeholder' => 'Description', 'class' => 'form-control']) !!}
        @error('description')
        <span class="error-message" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>
