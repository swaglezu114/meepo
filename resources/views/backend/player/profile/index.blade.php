@extends('backend.player.master')
@section('content')
    <main id="main">
        <!-- Main Page of Player Dashboard-->
        <!-- ======= Breadcrumbs Section ======= -->
        <section class="breadcrumbs">
            <div class="container">

                <div class="d-flex justify-content-between align-items-center">
                    <h1>Welcome to Meepo </h1>
                    <ol>
                        <li><a href="{{ route('player.dashboard') }}">Home</a></li>
                        <li>Meepo User</li>
                    </ol>
                </div>

            </div>
        </section><!-- End Breadcrumbs Section -->

    </main>
    <profile>
        <div class="container emp-profile">
            <div class="row">
                <div class="col-md-4">
                    <div class="profile-img">
                        @if(isset(auth()->user()->image))
                            <img src="{{ asset('uploads/user/'.auth()->user()->image) }}" alt=""/>
                        @else
                            <img src="{{ asset('player/asset/img/csgo.png') }}" alt=""/>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="profile-head">
                        <h2>{{ auth()->user()->name }}</h2>
                    </div>
                    {!! Form::model($data['row'], ['route' => ['player.profile.update',$data['row']->id], 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
                    @csrf
                    <div class="form-group">
                        <label for="image">Image</label>
                        {!! Form::file('main_image', ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        <label for="name">Name</label>
                        {!! Form::text('name', null, ['placeholder' => 'Name', 'class' => 'form-control', 'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        {!! Form::number('phone', null, ['placeholder' => 'Phone', 'class' => 'form-control', 'min' => 0]) !!}
                    </div>
                    <div class="form-group">
                        <label for="profession">Profession</label>
                        {!! Form::text('profession', null, ['placeholder' => 'Profession', 'class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <label for="khalti">Khalti ID</label>
                        {!! Form::number('khalti', null, ['placeholder' => 'Khalti ID', 'class' => 'form-control', 'min' => 0]) !!}
                    </div>
                    <div class="card-footer text-center">
                        <button type="submit" class="btn btn-info"><i class="fa fa-check"></i> Update profile
                        </button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </profile>
@endsection
