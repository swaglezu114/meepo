<nav id="nav-menu-container">
    <!-- Main Navbar of Player page-->
    <ul class="nav-menu">
        <li class="logo">
            <a href="/"><img src="{{ asset('player/asset/img/meepo.png') }}" width="150" alt="Sport Template"></a>
        </li>
        @guest
        @else
            <li><a href="{{ route('player.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('player.chat') }}">Chat</a></li>
            <li><a href="{{ route('player.team') }}">Team</a></li>
            <li><a href="{{ route('player.tournament') }}">Tournament</a></li>
            <li><a href="{{ route('player.shop') }}">Shop</a></li>
            <li><a href="">Coins: {{ auth()->user()->number_of_coins }}</a></li>
            <li>
                <a href="#" class="profile-edit-btn"
                   onclick="event.preventDefault();$('#logout-form').submit();">
                    <i class="fa fa-sign-out-alt"></i> Logout
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST">
                    @csrf
                </form>
            </li>
        @endguest
    </ul>
</nav>
