@extends('backend.player.master')
@section('content')
    <main id="main">
        <!-- All pages of the Player-->
        <!-- ======= Breadcrumbs Section ======= -->
        <section class="breadcrumbs">
            <div class="container">

                <div class="d-flex justify-content-between align-items-center">
                    <h1>Welcome to Meepo </h1>
                    <ol>
                        <li><a href="{{ route('player.dashboard') }}">Home</a></li>
                        <li>Meepo User</li>
                    </ol>
                </div>

            </div>
        </section><!-- End Breadcrumbs Section -->

    </main>
    <profile>
        <div class="container emp-profile">
            <div class="row">
                <div class="col-md-4">
                    <div class="profile-img">
                        @if(isset(auth()->user()->image))
                            <img src="{{ asset('uploads/user/'.auth()->user()->image) }}" alt=""/>
                        @else
                            <img src="{{ asset('player/asset/img/csgo.png') }}" alt=""/>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="profile-head">
                        <h3>{{ auth()->user()->name }}</h3>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                   aria-controls="home" aria-selected="true">About</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="feed-tab" data-toggle="tab" href="#shopping" role="tab"
                                   aria-controls="feed" aria-selected="false">Shopping</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="feed-tab" data-toggle="tab" href="#feed" role="tab"
                                   aria-controls="feed" aria-selected="false">Feed</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="feed-tab" data-toggle="tab" href="#tournament" role="tab"
                                   aria-controls="feed" aria-selected="false">Tournament</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <a href="{{ route('player.profile',auth()->user()->id) }}" class="profile-edit-btn">Edit
                        Profile</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                </div>
                <div class="col-md-8">
                    <div class="tab-content profile-tab" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Name</label>
                                </div>
                                <div class="col-md-6">
                                    <p>{{ auth()->user()->name }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Email</label>
                                </div>
                                <div class="col-md-6">
                                    <p>{{ auth()->user()->email }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Phone</label>
                                </div>
                                <div class="col-md-6">
                                    <p>{{ auth()->user()->phone }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Profession</label>
                                </div>
                                <div class="col-md-6">
                                    <p>{{ auth()->user()->profession }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Khalti ID</label>
                                </div>
                                <div class="col-md-6">
                                    <p>{{ auth()->user()->khalti }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="shopping" role="tabpanel" aria-labelledby="feed-tab">
                            <div class="row">
                                <div class="col-md-12">
                                    @foreach($data['userShop'] as $userShopItems)
                                        <h6>You have purchased <strong>{{ $userShopItems->shop->title }}</strong> from
                                            shop</h6>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="feed" role="tabpanel" aria-labelledby="feed-tab">
                            <div class="row">
                                <div class="col-md-12">
                                    @if(isset($data['userTournament']))
                                        @foreach($data['userTournament'] as $userTournament)
                                            <h6>You have won <strong>{{ $userTournament->coin }}</strong>
                                                amount from <strong>{{ $userTournament->tournament->name }}</strong>
                                            </h6>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tournament" role="tabpanel" aria-labelledby="feed-tab">
                            <div class="row">
                                <div class="col-md-12">
                                    @if(isset($data['teamUser']))
                                        <h6>You have joined tournament at
                                            <strong>{{ $data['teamUser']->created_at->format('Y-m-d') }}</strong></h6>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </profile>
@endsection
