<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>{{ config('app.name') }}</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Facebook Opengraph integration: https://developers.facebook.com/docs/sharing/opengraph -->
    <meta property="og:title" content="">
    <meta property="og:image" content="">
    <meta property="og:url" content="">
    <meta property="og:site_name" content="">
    <meta property="og:description" content="">

    <!-- Twitter Cards integration: https://dev.twitter.com/cards/  -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="">

    <!-- Favicons -->
    <link href="{{ asset('player/asset/img/favicon.png') }}" rel="icon">
    <link href="{{ asset('player/asset/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,700|Roboto:400,900" rel="stylesheet">


    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset('assets/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet"
          href="{{ asset('assets/backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">



    <!-- Vendor CSS Files -->
    <link href="{{ asset('player/asset/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('player/asset/venobox/venobox.css') }}&quot; rel=&quot;stylesheet">
    <link href="{{ asset('player/asset/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

@yield('css')
<!-- Template Main CSS File -->

    <link rel="stylesheet" href="{{ asset('player/asset/css/style3.css') }}"/>
    <!-- =======================================================
    * Name: Meepo - v1.0.0
    *
    * Author: Susan Wagle
    * License: Meepo by Susan Wagle
    ======================================================== -->

    <style>
        .error-message {
            width: 100%;
            margin-top: .25rem;
            font-size: 80%;
            color: #dc3545;
        }
    </style>

    @livewireStyles
</head>

<body>

<!-- ======= Header ======= -->
<header id="header">
    <div class="container">
    @include('backend.player.shared.navbar')
    <!-- #nav-menu-container -->

        <nav class="nav social-nav pull-right d-none d-lg-inline">
            <a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i
                    class="fa fa-linkedin"></i></a> <a href="#"><i class="fa fa-envelope"></i></a>
        </nav>
    </div>
</header><!-- End Header -->

<!-- End #main -->


@yield('content')


<!-- ======= Footer ======= -->
<footer class="site-footer" style="padding-top: 224px; background-color: white;">
    <div class="bottom">
        <div class="container">
            <div class="row">

                <div class="col-lg-6 col-xs-12 text-lg-left text-center">
                    <p class="copyright-text">
                        &copy; Copyright <strong>MEEPO</strong>. All Rights Reserved
                    </p>
                    <div class="credits">
                        Created by <a href="https://bootstrapmade.com/">Susan Wagle</a>
                    </div>
                </div>

                <div class="col-lg-6 col-xs-12 text-lg-right text-center">
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <a href="index.html">Home</a>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</footer><!-- End Footer -->

<a class="scrolltop" href="#"><span class="fa fa-angle-up"></span></a>

@include('sweetalert::alert')

@yield('js')


<!-- Vendor JS Files -->
<script src="{{ asset('player/asset/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('player/asset/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('player/asset/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
<script src="{{ asset('player/asset/vendor/php-email-form/validate.js') }}"></script>
<script src="{{ asset('player/asset/vendor/counterup/counterup.min.js') }}"></script>
<script src="{{ asset('player/asset/vendor/tether/js/tether.min.js') }}"></script>
<script src="{{ asset('player/asset/vendor/jquery-sticky/jquery.sticky.js') }}"></script>
<script src="{{ asset('player/asset/vendor/venobox/venobox.min.js') }}"></script>
<script src="{{ asset('player/asset/vendor/lockfixed/jquery.lockfixed.min.js') }}"></script>
<script src="{{ asset('player/asset/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('player/asset/vendor/superfish/superfish.min.js') }}"></script>
<script src="{{ asset('player/asset/vendor/hoverIntent/hoverIntent.js') }}"></script>
<script src="{{ asset('player/asset/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('player/asset/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- Template Main JS File -->
<script src="{{ asset('player/asset/js/main.js') }}"></script>

@livewireScripts
</body>

</html>
