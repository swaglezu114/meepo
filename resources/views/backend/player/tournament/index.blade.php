@extends('backend.player.master')
@section('content')
    <section class="breadcrumbs" id="middle1">
        <div class="container" id="middle2">

            <div class="d-flex justify-content-between align-items-right">
                <h1>Registered Tournament</h1>
                <ol>
                    <li><a href="{{ route('player.dashboard') }}">Home</a></li>
                    <li>Meepo Tournament</li>
                </ol>
            </div>

        </div>
    </section>
    <div class="container2">
        <div class="col-lg-9">
            <form action="{{ route('player.tournament') }}">
                <input type="text" name="tournament" class="form-control" placeholder="Search tournament...">
                <button hidden type="submit">Search</button>
            </form>
            <table class="table">
                <thead>
                <tr>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Game Image</th>
                    <th>Game</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data['tournaments'] as $tournament)
                    <tr>
                        <td>
                            @if(isset($tournament->image))
                                <img src="{{ asset('uploads/tournament/'.$tournament->image) }}" width="45" alt="">
                            @else
                                N/A
                            @endif
                        </td>
                        <td>{{ $tournament->name }}</td>
                        <td>
                            @if(isset($tournament->game_image))
                                <img src="{{ asset('uploads/tournament/'.$tournament->game_image) }}" width="45"
                                     alt="">
                            @else
                                N/A
                            @endif
                        </td>
                        <td>{{ $tournament->game }}</td>
                        
                        <td>
                            <a href="{{ route('player.join.tournament',$tournament->id) }}"
                               style="color: darkgreen">join
                                tournament</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $data['tournaments']->links() }}
        </div>
    </div>
@endsection
