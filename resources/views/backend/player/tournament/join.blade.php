@extends('backend.player.master')
@section('content')
    <section class="breadcrumbs" id="middle1">
        <div class="container" id="middle2">
            <div class="d-flex justify-content-between align-items-right">
                <h1>Registered Tournament</h1>
                <ol>
                    <li><a href="{{ route('player.dashboard') }}">Home</a></li>
                    <li>Meepo Tournament</li>
                </ol>
            </div>
        </div>
    </section>
    <div class="container2">
        <div class="col-lg-9">
            <table class="table">
                <tr>
                    <th>Image</th>
                    <td>
                        @if(isset($data['row']->image))
                            <img src="{{ asset('uploads/tournament/'.$data['row']->image) }}" width="75" alt="">
                        @else
                            N/A
                        @endif
                    </td>
                </tr>
                <tr>
                    <th>Name</th>
                    <td>{{ $data['row']->name }}</td>
                </tr>
                <tr>
                    <th>Game Image</th>
                    <td>
                        @if(isset($data['row']->game_image))
                            <img src="{{ asset('uploads/tournament/'.$data['row']->game_image) }}" width="75" alt="">
                        @else
                            N/A
                        @endif
                    </td>
                </tr>
                <tr>
                    <th>Game</th>
                    <td>{{ $data['row']->game }}</td>
                </tr>
            </table>
            <form action="{{ route('player.join.tournament.store') }}" method="POST">
                @csrf
                <input type="hidden" name="team_id" value="{{ $data['myTeam']->team_id }}">
                <input type="hidden" name="tournament_id" value="{{ $data['row']->id }}">
                <div class="form-group card-footer text-center">
                    <button class="btn btn-secondary">join tournament</button>
                </div>
            </form>
        </div>
    </div>
@endsection
