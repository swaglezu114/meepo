@extends('backend.player.master')
@section('content')
    <section class="breadcrumbs" id="middle1">
        <div class="container" id="middle2">

            <div class="d-flex justify-content-between align-items-center">
                <h1>Meepos Shop</h1>
                <ol>
                    <li><a href="{{ route('player.dashboard') }}">Home</a></li>
                    <li>Meepos Shop</li>
                </ol>
            </div>

        </div>
    </section>

    <div class="container2">
        <div class="col-lg-9">

            <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <img class="d-block img-fluid" src="{{ asset('player/asset/img/steam.png') }}" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block img-fluid" src="{{ asset('player/asset/img/steam2.png') }}" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block img-fluid" src="{{ asset('player/asset/img/steam3.png') }}" alt="Third slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

                <br>
                <br>
            </div>

            <div class="row">

                @if(isset($data['shops']))
                    @foreach($data['shops'] as $coin)
                        <div class="col-lg-4 col-md-6 mb-4">
                            <div class="card h-100">
                                @if(isset($coin->image))
                                    <a href="#"><img class="card-img-top"
                                                     src="{{ asset('uploads/shop/'.$coin->image) }}" alt=""></a>
                                @else
                                    <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
                                @endif

                                <div class="card-body">
                                    <h4 class="card-title">
                                        <a href="#">{{ $coin->title }}</a>
                                    </h4>
                                    <h5>Ec. {{ $coin->price }}</h5>
                                    <p class="card-text">{!! \Illuminate\Support\Str::words($coin->description,10) !!}</p>
                                </div>
                                <form action="{{ route('player.buy') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="costCoin" value="{{ $coin->price }}">
                                    <input type="hidden" name="shop_id" value="{{ $coin->id }}">
                                    <button class="btn-block" type="submit">buy</button>
                                </form>
                                <div class="card-footer">
                                    <small class="text-muted">&#9733; &#9733; &#9733; &#9734; &#9734;</small>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif

            </div>
        </div>
    </div>
@endsection
