@extends('backend.player.master')
@section('content')
    <main id="main">

        <!-- ======= Breadcrumbs Section ======= -->
        <section class="breadcrumbs">
            <div class="container">

                <div class="d-flex justify-content-between align-items-center">
                    <h1>Welcome to Meepo </h1>
                    <ol>
                        <li><a href="{{ route('player.dashboard') }}">Home</a></li>
                        <li>Meepo User</li>
                    </ol>
                </div>

            </div>
        </section><!-- End Breadcrumbs Section -->

    </main>
    <profile>
        <div class="container emp-profile">
            <div class="card">
                <div class="card-header">
                    Chat box
                </div>
                <div class="card-body">
                    @foreach($data['messages'] as $message)
                        <strong>{{ $message->user_id->name }} :</strong> {{ $message->message }} <br>
                    @endforeach
                    <livewire:chat-component/>
                </div>
            </div>
        </div>
    </profile>
@endsection
