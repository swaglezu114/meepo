@extends('backend.player.master')
@section('content')
    <section class="breadcrumbs" id="middle1">
        <div class="container" id="middle2">

            <div class="d-flex justify-content-between align-items-right">
                <h1>My Team</h1>
                <ol>
                    <li><a href="{{ route('player.dashboard') }}">Home</a></li>
                    <li>Meepo Tournament</li>
                </ol>
            </div>

        </div>
    </section>
    <div class="container2">
        <div class="col-lg-9">
            <div class="card">
                <div class="card-header">
                    <h5>Join team</h5>
                </div>
                <div class="card-body">
                    <table class="table">
                        <tr>
                            <th>Logo</th>
                            <td>
                                @if(isset($data['row']->logo))
                                    <img src="{{ asset('uploads/team/'.$data['row']->logo) }}" width="100"
                                         alt="">
                                @else
                                    N/A
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Name</th>
                            <td>
                                {{ $data['row']->name }}
                            </td>
                        </tr>
                        @if($data['row']->users->count() > 0)
                            <tr>
                                <th>Members</th>
                                <td>
                                    @foreach($data['row']->users as $mem)
                                        {{ $mem->name }}@if(!$loop->last), @endif
                                    @endforeach
                                </td>
                            </tr>
                        @endif
                    </table>
                    @if($data['row']->users->count() < 5)
                        <form action="{{ route('player.join.team.store') }}" method="POST"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <input type="hidden" name="team_id" value="{{ $data['row']->id }}" class="form-control">
                            </div>
                            <div class="form-group text-center card-footer">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>Join Team
                                </button>
                            </div>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="container2">
        <div class="col-lg-9">
        </div>
    </div>
@endsection
